#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QMessageBox>
#include <QMouseEvent>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    int x = 0;
    int y = 0;
    bool flag = false;
    QPainter chess(this);
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (flag == false)
            {
                chess.setBrush(Qt::white);
                flag = true;
            }
            else
            {
                chess.setBrush(Qt::black);
                flag = false;
            }
            QRect rectmin(x,y,50,50);
            chess.drawRect(rectmin);
            x+=50;
        }
        x = 0;
        y += 50;
        if (flag== false)
        {
            flag = true;
        }

        else
        {
            flag = false;
        }
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    int mesto1 = 8-(event->y()/50);
    int mesto2 = event->x()/50;
    char q;
    QMessageBox Message;

    switch(mesto2)
    {
        case 0:
            q = 'a';
            break;
    case 1:
        q = 'b';
        break;
    case 2:
        q = 'c';
        break;
    case 3:
        q = 'd';
        break;
    case 4:
        q = 'e';
        break;
    case 5:
        q = 'f';
        break;
    case 6:
        q = 'g';
        break;
    case 7:
        q = 'h';
        break;
    }

    Message.setText((QString("%1")+QString("%2")).arg(q).arg(mesto1));
    Message.exec();
}

