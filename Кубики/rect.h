#ifndef RECT_H
#define RECT_H
#include<QPainter>

class Rect
{
public:
    Rect();
    Rect(int m_x, int m_y, int m_w, int m_color);
    int x, y, w, color;
    void draw(QPainter *painter);
};

#endif // RECT_H
