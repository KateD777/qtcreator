#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QPainter>
#include<QMouseEvent>
#include <QMessageBox>
#include <QRect>
#include "rect.h"
using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void newLine();
    void defeat();


private:
    Ui::MainWindow *ui;
    //vector<Rect[5]> game;
    vector<Rect> blocks;
    Rect player = Rect(0, 360, 60, 11);
    int spead = 4;
    int points = 0;
    bool play = true;
public slots:
    void update();
};
#endif // MAINWINDOW_H
