#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);

    for(int i=0; i<9; i++)
    newLine();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event){
    QPainter painter(this);

    /*for(int i=0; i<game.size(); i++)
        for(int j=0; j<5; j++)
           game[i][j].draw(&painter);*/

    painter.drawRect(QRect(0, 0, 300, 540));
    player.draw(&painter);
    for(int j=0; j<blocks.size(); j++)
    blocks[j].draw(&painter);

    painter.drawText(500, 10, QString("Очки: %1").arg(points));
}
void MainWindow::mousePressEvent(QMouseEvent *event){
    if(event->x()<300){
        player.x=event->x()/60*60;
    }
    if(player.color == blocks[player.x/60].color){
        while (player.color == blocks[player.x/60].color){
            for(int j=0; j<5; j++)
                blocks.erase(blocks.begin());
            newLine();
            points++;
        }
        player.color = rand()%5+8;
    }

}

void MainWindow::newLine()
{
    int i;
    QList<int> colour = {8, 9, 10, 11, 12};
    if(blocks.size()!=0)
        i = blocks.back().y +60;
    else i=300;
    for(int j=0; j<5; j++) {
        int col = rand()%colour.size();
        blocks.push_back(Rect(j*60, i, 60, colour[col]));
        colour.removeAt(col);
    }
}

void MainWindow::defeat()
{
    QMessageBox message;
    message.setText("Проигрыш");
    message.exec();
}

void MainWindow::update()
{
    if(play){
        for(int j=0; j<blocks.size(); j++)
        blocks[j].y-=spead;
        if(blocks[0].y>540){
            int k = blocks[0].y - 540;
            for(int j=0; j<blocks.size(); j++){
               blocks[j].y-=k;
            }
        }
        if(blocks[0].y == 0) {
            defeat();
            play = false;
        }

        player.y=min(blocks[0].y-60, 480);
        repaint();
    }
}


