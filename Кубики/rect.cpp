#include "rect.h"

Rect::Rect()
{

}

Rect::Rect(int m_x, int m_y, int m_w, int m_color)
{
    x=m_x;
    y=m_y;
    w=m_w;
    color = m_color;

}

void Rect::draw(QPainter *painter)
{
    painter->setBrush(Qt::GlobalColor(color));
    painter->setPen(Qt::black);
    painter->drawRect(x, y, w, w);
}
