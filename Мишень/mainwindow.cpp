#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QMouseEvent>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    for (int i = 0; i < 10; i++)
    {
        circles[i]->draw(&painter);
    }

    painter.setPen(QPen(Qt::red,3));

    for (int i = 0; i < hit; i++)
    {
        painter.drawPoint(*shots[i]);
    }

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();
    shots[hit] = new QPoint(x, y);
    hit++;
    if (hit == 5)
    {
        for (int i = 0; i < 5; i++)
        {
            int k=0;

            for (int j = 0; j < 10; j++)
            {
                if (circles[j]->in_circle(shots[i]->x(), shots[i]->y()))
                {
                    k++;
                }
            }
        points+=k;
        }
        QMessageBox SMS;
        SMS.setText(QString("%1").arg(points));
        SMS.exec();
    }

    repaint();
}

