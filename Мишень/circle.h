#ifndef CIRCLE_H
#define CIRCLE_H
#include <QPainter>

class Circle
{
public:
    Circle(int m_x, int m_y, int m_radius);
    int x, y, radius;
    void draw (QPainter *painter);
    bool in_circle (int X, int Y);
};

#endif // CIRCLE_H
