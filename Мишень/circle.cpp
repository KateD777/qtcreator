#include "circle.h"
#include <cmath>

Circle::Circle(int m_x, int m_y, int m_radius)
{
    x = m_x;
    y = m_y;
    radius = m_radius;
}

void Circle::draw(QPainter *painter)
{
    painter->drawEllipse(x-radius, y-radius,radius*2, radius*2);
}

bool Circle::in_circle(int X, int Y)
{
    return pow(x-X,2)+pow(y-Y,2)<pow(radius,2);
}
