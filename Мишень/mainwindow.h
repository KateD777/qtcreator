#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<circle.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    QPoint* shots[5];
    int hit = 0;
    int points = 0;
    Circle* circles[10] = {new Circle (250, 250, 50),
                          new Circle (250, 250, 70),
                          new Circle (250, 250, 90),
                          new Circle (250, 250, 110),
                          new Circle (250, 250, 130),
                          new Circle (250, 250, 150),
                          new Circle (250, 250, 170),
                          new Circle (250, 250, 190),
                          new Circle (250, 250, 210),
                          new Circle (250, 250, 230)};

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
