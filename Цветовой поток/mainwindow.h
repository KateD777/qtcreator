#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void drawing(int x, int y, int r);

private:
    Ui::MainWindow *ui;
    int potok[12][12];
    int step = 0;
    int storona = 50;
    int pole = 12;
};
#endif // MAINWINDOW_H
