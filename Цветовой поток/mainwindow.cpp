#include "mainwindow.h"
#include "ui_mainwindow.h"
//цветной поток
#include<QPainter>
#include <QMouseEvent>
#include <QMessageBox>
#include <QPoint>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int i=0; i < pole; i++)
    {
        for (int j = 0; j < pole; j++)
        {
            potok[i][j] = rand() % 6+7;
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawing(int x, int y, int a)
{
    if (x<0 || y<0 || x>11 || y>11) //границы массива
        return;
    if (x<11 && potok[x+1][y] == potok[x][y])
        drawing(x+1, y, a);
    if (y<11 && potok[x][y+1] == potok[x][y])
        drawing(x, y+1, a);
    potok[x][y] = a;
    return;

}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(Qt::black);
    int flag = 0;
    int x= 0, y = 0;
    for (int i = 0; i < pole; i++)
    {
        for (int j = 0; j < pole; j++)
        {
            painter.setBrush(Qt::GlobalColor(potok[i][j]));
            painter.drawRect(QRect(x,y,storona,storona));
            x += storona;
        }
        x = 0;
        y += storona;
        flag++;
    }
        painter.drawText(10, 630, QString("Ход %1").arg(step));

        for (int i = 0; i< (pole/2); i++)
        {
            painter.setBrush(Qt::GlobalColor(i+7));
            painter.drawRect(QRect(i*storona, 13 * storona, storona, storona));  //палитра для игры
        }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    QMessageBox message;
    QMessageBox::StandardButton Q;
    int color = -1;

    for (int i = 0; i < (pole/2); i++)
    {
        if (QRect(i*storona, 13 * storona, storona, storona).contains(QPoint( event->x(), event->y() )) )   //чтобы палитра перекрашивания
            color = i + 7;
    }

    if (color != -1)
        drawing(0, 0, color);
    repaint();

    step++;
    if (step==22)
    {
        Q = QMessageBox::question(this, "Вопрос","Вы проиграли! Хотите продолжить игру?", QMessageBox::Yes|QMessageBox::No);
        if (Q == QMessageBox::No)
            QApplication::quit();
        else
            qDebug() << "Продолжаем игру";
        //message.setText("Вы проиграли! Попробуйте еще раз");
        //message.exec();
    }

    bool check_point;
    for (int i = 0; i < pole; i++)
    {
        for (int j = 0; j < pole; j++)
        {
            if (potok[i][j] != potok[0][0])
                check_point = false;
            if (potok[i][j] == potok[i-1][j] && potok[i][j] == potok[i][j-1] && potok[i-1][j] == potok[i][j-1]
                    && potok[i][j-1] == potok[i-1][j] && potok[i][j] == potok[j][i] && potok[i-1][j] == potok[i][j-1])
                check_point = true;
        }
    }


    if (check_point)
    {
        message.setText("Вы победили");
        message.exec();
    }

}



