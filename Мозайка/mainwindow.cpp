#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rect.h"
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *)//даем кисть
{
    QPainter painter(this);
    for (int i = 0; i < count; ++i) //используем тк у нас массив
    {
        rect[i]->draw(&painter);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->modifiers() & Qt::ControlModifier)    //созание прямоугольников при ctrl-click
    {
        if (count >= 10)
            return;
        if (n == 0)
        {
            x0 = event->x();   //по клику получаем координаты
            y0 = event->y();
            n++;
        }
        else
            {
                rect[count++] = new Rect(x0, y0, event->x(), event->y());   //записываем его в массив
                n = 0; //обнуляем для отрисовки следующего
            }
    }
    else
    {
        int x = event->x(), y = event->y(), maxX = 10000, minX = 0, maxY = 10000, minY = 0; //исходные данные для сравниений
        int h = 0;  //счетчик количества прямоугольников за клик
        for (int i = 0; i < count; ++i)
        {
            if (rect[i]->contains(x,y)) //проверям попали ли мы в другой прямоугольник
            {
                h++;
                if (rect[i]->getX1() >= minX)
                {
                    minX = rect[i]->getX1();
                }
                if (rect[i]->getX2() <= maxX)
                {
                    maxX = rect[i]->getX2();
                }
                if (rect[i]->getY1() >= minY)
                {
                    minY = rect[i]->getY1();
                }
                if (rect[i]->getY2() <= minY)
                {
                    maxY = rect[i]->getY2();
                }
            }

        }
        if (h >=2)
        {
            //работает, если нажать 1 раз, без ctrl на пересекаемую область
            std::cout << "Количество пересекаемых прямоугольников: "<< h <<std::endl;
            std::cout << "Площадь пересекаемых прямоугольников: " << (maxY-minY) * (maxX-minX) << std::endl;
        }
    }

    repaint();
}





