#include "rect.h"

Rect::Rect()
{

}

Rect::Rect(int x1, int y1, int x2, int y2) //сравниваем и передаем переменные (чтобы в методе не возникало неопределенности когда поле класса и параметры метода имеют одиноковое имя)
{
    if (x1<= x2)
    {
        this->x1 = x1;
        this->x2 = x2;
    }
    else
    {
        this->x2 = x1;   //можно использовать swap
        this->x1 = x2;
    }
    if (y1<= y2)
    {
        this->y1 = y1;
        this->y2 = y2;
    }
    else
    {
        this->y2 = y1;
        this->y1 = y2;
    }
}
//get и set
int Rect::getX1() const
{
    return x1;
}

void Rect::setX1(int newX1)
{
    x1 = newX1;
}

int Rect::getY1() const
{
    return y1;
}

void Rect::setY1(int newY1)
{
    y1 = newY1;
}

int Rect::getX2() const
{
    return x2;
}

void Rect::setX2(int newX2)
{
    x2 = newX2;
}

int Rect::getY2() const
{
    return y2;
}

void Rect::setY2(int newY2)
{
    y2 = newY2;
}

bool Rect::contains(int x, int y)
{
 return ( (x1<x) && (x<x2) && (y1<y) && (y<y2) );
}

void Rect::draw(QPainter *painter)
{
    painter->drawRect( QRect(x1, y1, x2-x1, y2-y1) );//даем кисть
}
