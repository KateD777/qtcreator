#ifndef RECT_H
#define RECT_H
#include <QPainter>

class Rect
{
public:
    Rect();
    Rect(int x1, int y1, int x2, int y2); //как задаются параметры прямоугольника
    int getX1 () const;   //getы и setры
    void setX1(int newX);

    int getY1 () const;
    void setY1(int newY);

    int getX2 () const;
    void setX2(int newH);

    int getY2 () const;
    void setY2(int newW);

    bool contains(int x, int y);   //хранение координат для клика
    void draw(QPainter *painter);//для рисования прямоугольника

private:
    int x1, x2, y1, y2;  //координаты прямоугольника
};

#endif // RECT_H
