#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::intersect_circle(QLine line, QPoint circle, int r)
{
    for(int i = std::min(line.x1(), line.x2()); i<=std::max(line.x2(), line.x1()); i++){
        for(int j = std::min(line.y1(), line.y2()); j<=std::max(line.y2(), line.y1()); j++){
            if (!in(QPoint(i, j), circle, r)){
                return true;
            }
        }
    }
    return false;
}

bool MainWindow::in(QPoint p, QPoint circle, int r)
{
    return ((p.x()-circle.x())*(p.x()-circle.x())+(p.y()-circle.y())*(p.y()-circle.y())<r*r);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    /*painter.drawLine(linef);
    painter.drawLine(perpend(linef));
    painter.setPen(QPen(Qt::black, 5));
    painter.drawPoint(QPoint(100, 100));*/





    for(int i = 0; i<n; i++){
        painter.drawLine(QPoint(i*20,0), QPoint(i*20, n*20) );
         painter.drawLine(QPoint(0, i*20), QPoint(n*20, i*20) );
    }
    if(count!=0){
        painter.drawEllipse(circle);

        for(int i = circle.left()/20*20; i<circle.right(); i+=20)
            for(int j = circle.top()/20*20; j<circle.bottom(); j+=20){
               if((!intersect_circle(QLine(QPoint(i, j),QPoint(i+20, j)), circle.center(), circle.width()/2)) &&
                       (!intersect_circle(QLine(QPoint(i, j),QPoint(i, j+20)), circle.center(), circle.width()/2))&&
                       (!intersect_circle(QLine(QPoint(i+20, j+20),QPoint(i+20, j)), circle.center(), circle.width()/2))&&
                       (!intersect_circle(QLine(QPoint(i+20, j+20),QPoint(i, j+20)), circle.center(), circle.width()/2)))
                        {
                   painter.setBrush(Qt::blue);
                   painter.drawRect(i, j, 20, 20);
               }
            }
    }

    //painter.drawLine(QPoint(20,0), QPoint(20, contentsRect().y()));
}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(count==0){
        circle = QRect(event->x()-radius, event->y()-radius, radius*2, radius*2);
    }
    count++;
    repaint();
}

