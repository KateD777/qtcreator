#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int i = Height + kletka; i < Width; i += kletka)
        for (int j = Height + kletka; j < Width; j += kletka)
            points_to_draw.push_back(QPoint(i,j));

    Tree();
    repaint();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter (this);
    for (int i = Height; i<= Width; i += Width-Height/garden_size)
    {
        painter.drawLine(i, Height, i, Width);
        painter.drawLine(Height, i, Width,i);
    }

    painter.setBrush(Qt::green);
    painter.drawEllipse(QPoint(Height, Height),5,5);

    painter.setBrush(Qt::blue);
    for (auto a : points_to_draw)
        painter.drawEllipse(a,5,5);
}

void MainWindow::Tree()
{
    for (int i =0; i <points_to_draw.size(); i++)
    {
        for (int j = i+1; j < points_to_draw.size(); j++)
        {
            if ( ((points_to_draw[i].x()-Height)/kletka) * ((points_to_draw[j].y()-Height)/kletka) == ((points_to_draw[i].y()-Height)/kletka) * ((points_to_draw[j].x()-Height)/kletka))
                points_to_draw[j] = QPoint(-100,-100);
        }
    }
}

