#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);
    void Tree();

private:
    Ui::MainWindow *ui;

    QVector <QPoint> points_to_draw;
    int garden_size = 10;
    int kletka = 20;   // 40
    int Width = 250;  //500
    int Height = 50; //100
};
#endif // MAINWINDOW_H
