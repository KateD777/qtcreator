#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QPainter>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool intersect_circle(QLine line, QPoint circle, int r);
    bool in(QPoint p, QPoint circle, int r);
    void mousePressEvent(QMouseEvent *event);
    QLineF linef = QLine(QPoint(100, 200), QPoint(100, 100));

    void paintEvent(QPaintEvent *event);

    int count = 0;
    int radius = 100;
    std::vector <QRect> Circles;
    private:
        Ui::MainWindow *ui;
    };
    #endif // MAINWINDOW_H
