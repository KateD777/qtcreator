#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::in(QPoint p, QPoint circle, int r)
{
    return ((p.x()-circle.x())*(p.x()-circle.x())+(p.y()-circle.y())*(p.y()-circle.y())<r*r); // если растояние меньше радиуса то точка в круге
}

void MainWindow::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    for(int i=0; i<Circles.size(); i++){
        painter.drawEllipse(Circles[i]);
    }

    int  k = 0;
    for(int i=0; i<Circles.size(); i++){
        for(int x=Circles[i].left(); x<Circles[i].right(); x++){
            for(int y=Circles[i].top(); y<Circles[i].bottom(); y++){
                for(int j=0; j<Circles.size(); j++){
                    if(in(QPoint(x, y), Circles[i].center(), Circles[i].width()/2) && in(QPoint(x, y), Circles[j].center(), Circles[j].width()/2)){
                        k++;
                    }
                }
                if(k==2){
                    painter.setPen(QPen(Qt::red));

                }
                else if(k==3){
                    painter.setPen(QPen(Qt::yellow));

                }
                else if(k==4){
                    painter.setPen(QPen(Qt::green));

                }
                else if(k==5){
                    painter.setPen(QPen(Qt::blue));

                }
                else if(k>=6){
                    painter.setPen(QPen(Qt::black));

                }
                if(k>1)
                painter.drawPoint(x, y);
                k=0;
            }
        }
    }
}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
        Circles.push_back(QRect(event->x()-radius, event->y()-radius, radius*2, radius*2)); //ставим круг
    repaint();
}

