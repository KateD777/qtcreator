#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::paintEvent(QPaintEvent *)
{

    QPainter painter(this);
    for (auto a : points)
    {
        painter.drawEllipse(a,5,5);
     }
     painter.drawEllipse(provider, 10, 10);

     if (near != QPoint(0,0))
     {
         qDebug ("!");
         painter.drawLine(near, provider);
         for (auto a : points)
             painter.drawLine(near, a);
     }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (flag < 5)
    {
        points.push_back(event->pos());
        repaint();
        flag++;
    }
    else
    {
        provider = event->pos();
        findMin();
        repaint();
    }
}

void MainWindow::findMin()
{
    int dist = INT_MAX;
    int tmp;
    int s = 0;
    for (int i = 0; i < points.size(); i++)
    {
        for(int j = 0; j < points.size(); j++)
        {

        int tmp = pow((points[i].rx()- points[j].rx()), 2)  + pow((points[i].ry()- points[j].ry()), 2);  //вычисляем расстояние
        s +=tmp;
        }
        if (s < dist)
            near = points[i];
        s = 0;
    }
}






