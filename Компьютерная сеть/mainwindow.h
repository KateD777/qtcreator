#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QPoint>
#include <QPainter>
#include <QPaintEvent>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void paintEvent(QPaintEvent *event); //функция для отрисовки
    void mousePressEvent(QMouseEvent *event); //функции действий при нажатии кнопок мыши

    void findMin();
private:
    Ui::MainWindow *ui;

    QVector<QPoint> points;  //вектор точек
    QPoint near = QPoint(0,0);
    QPoint provider;

    int flag = 0;
};

#endif // MAINWINDOW_H
